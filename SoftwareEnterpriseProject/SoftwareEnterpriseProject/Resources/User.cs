﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SoftwareEnterpriseProject.Resources
{
    public class User
    {
        private int id;
        private string userName;
        private string email;
        private string password;
        private bool isPolice;
        private DateTime dob;
        private int logAttempts;


        public int Id { get => id; set => id = value; }
        public string UserName { get => userName; set => userName = value; }
        public string Email { get => email; set => email = value; }
        public string Password { get => password; set => password = value; }
        public bool IsPolice { get => isPolice; set => isPolice = value; }
        public DateTime Dob { get => dob; set => dob = value; }
        public int LogAttempts { get => logAttempts; set => logAttempts = value; }





        public User(string pUser, string pEmail, string pPassword)
        {
            userName = pUser;
            email = pEmail;
            password = pPassword;
            logAttempts = 0;
        }


        public void logFailed()
        {
            logAttempts++;
        }

    }
}