﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SoftwareEnterpriseProject.Helpers
{
    public class Helper
    {

        public static  MySqlConnection SetUpDB()
        {

            try
            {
                string conn = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

                MySqlConnection myConnection = new MySqlConnection(conn);

                return myConnection;
            }
            catch (Exception ex)
            {
                return null;
            }



        }
    }
}