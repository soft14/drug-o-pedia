﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SoftwareEnterpriseProject.LogIn.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="../CSS/LogIn-styles.css" />
    <title>Drug-O-Pedia</title>
</head>
<body>
    <form id="form1" runat="server">

        <section class="logo">
            <img src="../Images/Drug-O-Pedia_Logo.png"/>
        </section>


        <section class="section-line">
                        <div class="vertical-line"></div>
        </section>


        <section class="main-form">

            <div class="section-buttons">
                <asp:Button runat="server" ID="showLogIn" Text="LogIn" CssClass="buttons" OnClick="showLogIn_Click" />
                <p><a>&nbsp</a></p>
                <asp:Button runat="server" ID="ShowSignUp" Text="Sign Up" CssClass="buttons" OnClick="ShowSignUp_Click" />
            </div>


            <section runat="server" id="form_login" class="form-login">
                <h5>LogIn</h5>
                <input runat="server" class="controls" type="text" name="emailUser" placeholder="Email" id="userInputLogIn" value="" />
                <input runat="server" class="controls" type="password" name="password" placeholder="Password" id="passwordInputLogIn" value="" />
                <asp:Button CssClass="buttons" runat="server" ID="LogInbutton" Text="LogIn" OnClick="LogInbutton_Click" />
                <p><a href="#">Forgot your password?</a></p>
            </section>

            <section runat="server" class="form-signup" id="form_signup" visible="false">
                <h5>Sign Up</h5>
                <input runat="server" class="controls" type="text" name="username" placeholder="Username" id="usernameInputSignUp" value="" maxlength="25"/>
                <input runat="server" class="controls" type="text" name="email" placeholder="Email" id="emailInputSignUp" value="" maxlength="25"/>
                <input runat="server" class="controls" type="text" name="password" placeholder="Password    Min 8. Max 16 characters" id="passwordInputSignUp" value="" maxlength="16" />
                <input runat="server" class="controls" type="text" name="password" placeholder="Confirm your Password" id="repeatPasswordInputSignUp" value="" maxlength="16"/>
                <asp:Button CssClass="buttons" runat="server"  ID="SignUpbutton" Text="Sign Up" OnClick="SignUpbutton_Click"/>
            </section>
        </section>


    </form>
</body>
</html>
